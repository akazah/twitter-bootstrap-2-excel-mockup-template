# Twitter BootStrap 2 Excel mockup Template  #

This is a Excel sheet for make mockup of webpage with Twitter Bootstrap Framework.

## Creative Commons  ##
This contents under CC-SA.

@akazah http://akazah.com/ http://akazah.com/twitter-bootstrap-2-excel-mockup-template/

* * *

(now making English page.This page is Japanese one. To contact with me, Please use Twitter. My account is @akazah)

## Original Credits ##

Designed and built with all the love in the world @twitter by @mdo and @fat.
Code licensed under the Apache License v2.0. Documentation licensed under CC BY 3.0.
Icons from Glyphicons Free, licensed under CC BY 3.0.

Enjoy!
